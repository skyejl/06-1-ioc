package com.twuc.webApp.yourTurn;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleDependent {
    private String name;

    @Bean
    public void setName() {
        this.name = "O_o";
    }

    public String getName() {
        return name;
    }
}
