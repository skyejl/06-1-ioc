package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    private Dependency dependency;
    private String string;
    @Autowired
    public MultipleConstructor(Dependency dependency) {
        this.dependency = dependency;
    }
    public MultipleConstructor(String string) {
        this.string = string;
    }

    public Dependency getDependency() {
        return dependency;
    }

    public String getString() {
        return string;
    }
}
