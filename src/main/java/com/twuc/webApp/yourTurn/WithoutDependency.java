package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class WithoutDependency {
    private Dependent dependent;

    public WithoutDependency(Dependent dependent) {
        this.dependent = dependent;
    }

    public Dependent getDependent() {
        return dependent;
    }
}
