package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithAutowiredMethod {
    private Dependency dependency;
    private AnotherDependent anotherDependent;
    @Autowired
    public void initialize(AnotherDependent anotherDependent) {
        this.anotherDependent = anotherDependent;
    }

    public AnotherDependent getAnotherDependent() {
        return anotherDependent;
    }

    public Dependency getDependency() {
        return dependency;
    }
}
