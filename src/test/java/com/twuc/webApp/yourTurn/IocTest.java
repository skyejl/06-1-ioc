package com.twuc.webApp.yourTurn;

import com.twuc.webApp.OutOfScanningScope;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class IocTest {
    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void should_prepare_context() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
    }

    @Test
    void should_create_object_without_dependency() {
        WithoutDependency withoutDependency = context.getBean(WithoutDependency.class);
        assertNotNull(withoutDependency);
    }

    @Test
    void should_create_object_with_dependency() {
        WithoutDependency withoutDependency = context.getBean(WithoutDependency.class);
        assertNotNull(withoutDependency.getDependent());
        assertNotNull(withoutDependency);
    }

    @Test
    void shoule_create_object_fail_when_outOfScanScope() {
        assertThrows(RuntimeException.class, () ->{
            OutOfScanningScope outOfScanningScope = context.getBean(OutOfScanningScope.class);
        });
    }

    @Test
    void should_create_object_via_interface() {
        InterfaceImpl anInterface = context.getBean(InterfaceImpl.class);
        assertNotNull(anInterface);
    }

    @Test
    void should_create_object_via_configuration() {
        SimpleObject simpleInterface = (SimpleObject) context.getBean(SimpleInterface.class);
        assertNotNull(simpleInterface);
        assertEquals("O_o",simpleInterface.getSimpleDependent().getName());
    }

    @Test
    void should_choose_particular_constructor_via_autowired() {
        MultipleConstructor multipleConstructor = context.getBean(MultipleConstructor.class);
        assertNotNull(multipleConstructor);
        assertNotNull(multipleConstructor.getDependency());
        assertNull(multipleConstructor.getString());
    }

    @Test
    void should_apply_autowired_notation_to_initialize() {
        WithAutowiredMethod withAutowiredMethod = context.getBean(WithAutowiredMethod.class);
        assertNotNull(withAutowiredMethod);
        assertNotNull(withAutowiredMethod.getAnotherDependent());
        assertNull(withAutowiredMethod.getDependency());
    }

    @Test
    void should_create_array_of_interface() {
        String[] names = context.getBeanNamesForType(InterfaceWithMultipleImpls.class);
        assertNotNull(names);
    }
}
